 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-47419807-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-47419807-3');
</script>

![LOGO_RESCOMP](images/unimelb_logo_rescomp.jpg)

<h1> Welcome! </h1> 
<h5> This is Research Computing Services online self paced tutorial content. This is here to help you learn the digital research skills you need, while our face to face trainings are on hold. <p>
This content forms one part of our Digital Research Skills Support Pack. These tutorials are structured by the tools we teach. Each tool has a series of modules which are broken down based on skill level, including Introductory and Advanced. </h5>  
</h5>

<h1> Digital Research Skills Support Pack </h1>
You can find our Digital Research Skills Support Pack [click here](http://go.unimelb.edu.au/e7kr).
The pack includes other useful resources including e-learning videos and online trainings. 


<h1> What tools can I learn?? </h1>

* [ Python](https://gitlab.unimelb.edu.au/rescom-training/python)
*  [MATLAB](https://gitlab.unimelb.edu.au/rescom-training/MATLAB) 
*  [R](https://gitlab.unimelb.edu.au/rescom-training/r)
* [ Nvivo](https://gitlab.unimelb.edu.au/rescom-training/nvivo)
*  [3D Slicer](https://gitlab.unimelb.edu.au/rescom-training/ubuntu)
*  [Rhino](https://gitlab.unimelb.edu.au/rescom-training/3d-data-analysis/rhino) 
* [Omeka](https://gitlab.unimelb.edu.au/rescom-training/omeka)
*  [LaTex](https://gitlab.unimelb.edu.au/rescom-training/latex)
* [ Ubuntu](https://gitlab.unimelb.edu.au/rescom-training/ubuntu)

We highly reccomend you attend our online trainings & drop in sessions. 

<h1> How is training run & where can I sign up for training </h1> 
We are running trainings online via zoom & they are taught for researchers, by researchers! We highly reccomend you attend these trainings, this is a great way to get the skills you need and meet others in the community who can help you when you need it!

Sign up for training here: [rescomunimelb.eventbrite.com](rescomunimelb.eventbrite.com)


<h1> Help! I'm not sure which tool I need! </h1> 

We have videos on YouTube which explain how our tools can be used in research. [View them here](https://www.youtube.com/user/ResearchComputingServices_unimelb). <br>
You can also read each of the training descriptions, which describes what you will learn in each training, to help you decide whether it is for you. 

<h1> What other services can I access </h1>

You can access HPC, Cloud & Data Storage services, [check out our website here](https://research.unimelb.edu.au/infrastructure/research-computing-services)

<h1> I have more questions, how can I get in contact? </h1>

[Tweet Us](https://twitter.com/rescom_unimelb)

[Contact us via our website](https://research.unimelb.edu.au/infrastructure/research-computing-services#contact-us)

[Follow us on Instagram](instagram.com/rescom_unimelb)





